Spicy Deli
==================================

This Symonfy Application consists of RESTful API, API Tests and Unit Tests. The application has been developed considering the requirements of Spicy Deli's outlines for the project. The APIs will enable Spicy Deli to build mobile and desktop/web clients in future to enhance their online presence.


## PreRequistes

* PHP 7.*
* MySQL 5.7.*
* Git
* Composer


## Installation


### Application Setup

**Clone the repo**:
```bash
$ git clone https://vivek_qb@bitbucket.org/vivek_qb/catalog_system.git
```

**Change directory**:
```bash
$ cd catalog_system
```

**Install dependencies with composer**:
```bash
$ composer install
```
**Database Connection**:

Change the database connection in .env file and provide replace db_user, db_password, db_name, host and port with actual values.
```bash
$ DATABASE_URL="mysql://db_user:db_password@host:port/db_name?serverVersion=5.7
```

### Database Setup

**Create Database**:
```bash
php bin/console doctrine:database:create
```

**Create Schema**:
```bash
php bin/console doctrine:schema:update -f
```

**Load Seed Data**:
```bash
php bin/console doctrine:fixtures:load
```

### Server Setup
Setup a compatible web server (i.e. Apache, Nginx) with root directory targeting to Public directory of the application.

or

Start a local web server
```bash
$ symfony server:start
```
**Postman collection and environment**

The following collection and environment files available in the source root directory can be used to import in postman. Please change the url variable in the given environment as per your webserver.

```bash
Catalog System.postman_collection.json
```
```bash
Local.postman_environment.json
```

## API Endpoints

| Method        | URL           | Description  |
| ------------- |:-------------| :-----|
| **GET**       | /api/v1/ping | Health Check API |
| **POST**      | /api/v1/products | Create a product |
| **GET**       | /api/v1/products |List all products |
| **GET**       | /api/v1/products/:id | Retrieve a single product |
| **PUT**       | /api/v1/products/:id | Update all attributes of a product at once (i.e., replace a product)  |
| **PATCH**     | /api/v1/products/:id | Update one or more attributes of a product at once |
| **DELETE**    | /api/v1/products/:id | Delete a product |
| **GET**       | /api/v1/categories | List all product categories |

## Source Code

The developer generated code resides in the following php files: 

* **Controller**
	* src/Controller/HealthcheckController
	* src/Controller/CategoryController
	* src/Controller/ProductController
* **Entity**
	* src/Entity/Category
	* src/Entity/Product
* **Repository**
	* src/Repository/CategoryRepository
	* src/Repository/ProductRepository
* **Service**
	* src/Services/ProductService
* **Form**
	* src/Form/ProductType
* **Data Fixture**
	* src/DataFixtures/AppFixtures
* **Unit Test**
	* tests/unit/ProductTest
* **API Test**
	* tests/unit/ProductTestCest

##  API Testing Using Codeception

```bash
$ php vendor/bin/codecept run api
```

## Unit Testing Using Codeception
---------------------------

```bash
$ php vendor/bin/codecept run unit
```