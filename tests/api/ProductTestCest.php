<?php

namespace App\Tests;

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

class ProductTestCest
{
    private int $id;
    public function _before(ApiTester $I)
    {

    }

    /**
     * @param \App\Tests\ApiTester $I
     * @throws \Exception
     */
    public function createProduct(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/products', json_encode([
            "name" => "Product 1",
            "sku" => "SKU 2",
            "price" => 11.22,
            "categories" => [1, 2]
        ]));
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string',
            'sku' => 'string',
            'price' => 'float',
            'categories' => 'array|null',
            'created_at' => 'string'
        ]);

        $id = $I->grabDataFromResponseByJsonPath('$.id');
        if($id){
            $this->id = $id[0];
        }
    }

    /**
     * @param \App\Tests\ApiTester $I
     */
    public function replaceProduct(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut("/products/$this->id", json_encode([
            "name" => "Put - Product Name",
            "sku" => "PUT SKU",
            "price" => 9.99,
            "categories" => [1]
        ]));
        $I->seeResponseCodeIs(204);
    }

    /**
     * @param \App\Tests\ApiTester $I
     */
    public function updateProduct(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPatch("/products/$this->id", json_encode([
            "name" => "Patch - Product Name",
            "sku" => "Patch SKU",
        ]));
        $I->seeResponseCodeIs(204);
    }

    public function getProductById(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet("/products/$this->id");
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string',
            'sku' => 'string',
            'price' => 'float',
            'categories' => 'array|null',
            'created_at' => 'string',
            'updated_at' => 'string|null'
        ]);
    }

    /**
     * @param \App\Tests\ApiTester $I
     */
    public function deleteProduct(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete("/products/$this->id");
        $I->seeResponseCodeIs(204);
    }

}
