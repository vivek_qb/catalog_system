<?php
namespace App\Tests;

use App\Entity\Product;
use Symfony\Component\Validator\Validation;


class ProductTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    private $validator;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    public function testProductEnityIsCreatedWithValidInput()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setName('Valid Product Name');
        $product->setPrice(9.99);
        $product->setSku('ValidSKU');

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) == 0);
    }

    public function testProductEnityIsNotCreatedWithMissingName()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setPrice(9.99);
        $product->setSku('ValidSKU');

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) > 0);
    }

    public function testProductEnityIsNotCreatedWithMissingSku()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setName('Valid Product Name');
        $product->setPrice(9.99);

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) > 0);
    }

    public function testProductEnityIsNotCreatedWithMissingPrice()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setName('Valid Product Name');
        $product->setSku('ValidSKU');

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) > 0);
    }

    public function testProductEnityIsNotCreatedWithNameLessThanSixCharacter()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setName('Pro');
        $product->setPrice(9.99);
        $product->setSku('ValidSKU');

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) > 0);
    }
    public function testProductEnityIsNotCreatedWithNameMoreThanHundredCharacter()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $product = new Product();
        $product->setName('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut at posuere urna, at aliquam felis. Ut pulvinar');
        $product->setPrice(9.99);
        $product->setSku('ValidSKU');

        $errors = $this->validator->validate($product);
        $this->assertTrue(count($errors) > 0);
    }
}