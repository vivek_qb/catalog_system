<?php

namespace App\Services;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    private ProductRepository $productRepository;
    private RequestStack $requestStack;
    private FormFactoryInterface $form;
    private EntityManagerInterface $entityManager;
    private ?Request $request;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param RequestStack $requestStack
     * @param FormFactoryInterface $form
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ProductRepository $productRepository,
        RequestStack $requestStack,
        FormFactoryInterface $form,
        EntityManagerInterface $entityManager
    )
    {
        $this->productRepository = $productRepository;
        $this->requestStack = $requestStack;
        $this->form = $form;
        $this->entityManager = $entityManager;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @param int $id
     * @return Product
     */
    public function getProductById(int $id){
        $product = $this->productRepository->find($id);

        if(null === $product){
            throw new NotFoundHttpException();
        }

        return $product;
    }

    /**
     * @return Product[]
     */
    public function getAllProducts(){
        return  $this->productRepository->findAll();
    }

    /**
     * @param $product
     * @param bool $clearMissing
     * @return mixed|FormInterface
     */
    public function processForm($product, bool $clearMissing = true){
        $form = $this->form->create(ProductType::class, $product);
        $form->submit(
            $this->request->request->all(),
            $clearMissing
        );

        if(false == $form->isValid()){
            return $form;
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param $product
     * @return mixed|FormInterface
     */
    public function addProduct($product){
        return $this->processForm($product);
    }

    /**
     * @param $id
     * @return mixed|FormInterface
     */
    public function updateProduct($id)
    {
        $product = $this->getProductById($id);
        return $this->processForm($product);
    }

    /**
     * @param $id
     * @return mixed|FormInterface
     */
    public function patchProduct($id)
    {
        $product = $this->getProductById($id);
        return $this->processForm($product,false);
    }

    /**
     * @param $id
     * @return Product
     */
    public function deleteProduct($id): Product
    {
        $product = $this->getProductById($id);

        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return $product;
    }

}