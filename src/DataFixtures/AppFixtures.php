<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $seedData = '{
            "products": [{
                "name": "Sik Sik Wat",
                "category": "Ethiopia, Meat, Beef, Chili pepper",		
                "sku": "DISH999ABCD",
                "price": 13.49
            }, {
                "name": "Huo Guo",
                "category": "China, Meat, Beef, Fish, Tofu, Sichuan pepper",
                "sku": "DISH234ZFDR",
                "price": 11.99
            }, {
                "name": "Cau-Cau",
                "category": "Peru, Potato, Yellow Chili pepper",
                "sku": "DISH775TGHY",
                "price": 15.29
            }]
        }';

        $decodedData = json_decode($seedData);
        $productData = $decodedData->products;

        foreach($productData as $productObject){
            $product = new Product();
            $product->setName($productObject->name);
            $product->setSku($productObject->sku);
            $product->setPrice($productObject->price);

            $categories = $productObject->category;
            $categories = explode(',', $categories);
            if($categories){
                foreach($categories as $category){
                    $category = trim($category);
                    $categoryEntity = $this->categoryRepository->findByName($category);

                    if($categoryEntity){
                        $product->addCategory($categoryEntity[0]);
                    }else{
                        $categoryObject = new Category();
                        $categoryObject->setName($category);
                        $manager->persist($categoryObject);
                        $manager->flush();

                        $product->addCategory($categoryObject);
                    }
                }
            }

            $manager->persist($product);
            $manager->flush();
        }
    }
}
