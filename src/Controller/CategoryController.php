<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/v1", name="category_")
 */
class CategoryController extends AbstractFOSRestController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @Rest\Get("/categories", name="getAll")
     */
    public function getAllAction()
    {
        $categories = $this->categoryRepository->findAll();
        return $this->view(
            $categories,
            Response::HTTP_OK
        );
    }
}
