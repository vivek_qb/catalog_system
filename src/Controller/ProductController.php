<?php

namespace App\Controller;

use App\Entity\Product;
use App\Services\ProductService;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;


/**
 * @Route("/api/v1")
 */
class ProductController extends AbstractFOSRestController
{
    private ProductService $productService;

    public function __construct(
        ProductService $productService
    )
    {
        $this->productService = $productService;
    }

    /**
     * @Rest\Get("/products")
     */
    public function getAllAction(): View
    {
        $products = $this->productService->getAllProducts();

        return $this->view(
            $products,
            Response::HTTP_OK
        );
    }


    /**
     * @param $id
     * @return View
     * @Rest\Get("/products/{id}")
     */
    public function getByIdAction($id): View
    {
        $product = $this->productService->getProductById($id);

        return $this->view(
            $product,
            Response::HTTP_OK
        );
    }


    /**
     * @return View
     * @Rest\Post("/products")
     */
    public function postAction(): View
    {
        $product = new Product();
        $result = $this->productService->addProduct($product);

        if ($result instanceof Form) {
            return $this->view(
                $result,
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            $product,
            Response::HTTP_CREATED);
    }


    /**
     * @param $id
     * @return View
     * @Rest\Put("/products/{id}")
     */
    public function putAction($id): View
    {
        $result = $this->productService->updateProduct($id);

        if ($result instanceof Form) {
            return $this->view(
                $result,
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            null,
            Response::HTTP_NO_CONTENT);
    }


    /**
     * @param $id
     * @return View
     * @Rest\Patch("/products/{id}")
     */
    public function patchAction($id): View
    {
        $result = $this->productService->patchProduct($id);

        if ($result instanceof Form) {
            return $this->view(
                $result,
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->view(
            null,
            Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Delete("/products/{id}")
     */
    public function deleteAction($id): View
    {
        $this->productService->deleteProduct($id);

        return $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}
