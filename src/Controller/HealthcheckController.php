<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;

/**
 * @Route("/api/v1", name="healthcheck_")
 */
class HealthcheckController extends AbstractFOSRestController
{
    /**
     * @Get("/ping", name="healthcheck")
     */
    public function index(): JsonResponse
    {
        return new JsonResponse('pass');
    }
}
